#!/usr/bin/env -S bash -p
#------------------------------------------------------------------------------
# Description: Create symlinks from this repository to $HOME
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Dotfiles
# Last Modified: 2023-06-26
#------------------------------------------------------------------------------

shopt -s extglob

declare -r prg=${0##*/}

die ()
{
	echo "$prg: $2" >&2
	(( $1 > 0 )) &&
		exit $1
}

usage ()
{
	read -d '' <<-EOF
	Usage: $prg [OPTIONS]

	  -h, --help        - Display this help infomation.
	  -c, --copy        - Copies instead of symlinks.

	EOF
	echo -n "$REPLY"
}

install() { ln -sv "$@"; }

while [[ -n $1 ]]; do
	case $1 in
	-h|--help)
		usage
		exit 0 ;;

	-c|--copy)
		install() { cp -vnr "$@"; } ;;

	*)
		die 1 "$1: invaild option." ;;
	esac
	shift
done

base=$(realpath "$0")
declare -r base=${base%/*}/src

[[ -d $base ]] ||
	die 1 "$base: doesn't exist."

[[ -d $HOME/.config ]] ||
	mkdir "$HOME"/.config

[[ -d $HOME/.config/gtk-2.0 ]] ||
	mkdir "$HOME"/.config/gtk-2.0

[[ -d $HOME/.links ]] ||
	mkdir "$HOME"/.links

[[ -d $HOME/.icons ]] ||
	mkdir "$HOME"/.icons

[[ -d $HOME/.local/share/aria2 ]] ||
	mkdir -p "$HOME"/.local/share/aria2

for src in "$base"/.{bash*,dir_colors,config/!(gtk-2.0),icons/*,links/*} \
	"$base"/.config/gtk-2.0/gtkrc
{
	dest=$HOME/${src#$base/}

	[[ -e $dest ]] && {
		die 0 "${dest/$HOME/\~}: already existed."
		continue
	}

	find "$dest" -xtype l -delete &>/dev/null
	install "$src" "$dest"
}

exit 0
