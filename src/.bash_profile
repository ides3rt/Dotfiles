#!/usr/bin/env -S bash -p
#------------------------------------------------------------------------------
# Description: ~/.bash_profile
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Dotfiles
# Last Modified: 2022-09-11
#------------------------------------------------------------------------------

# Set default permissions to 'drwx------' and '-rw-------'.
# You may use 027 as a alternative (022 is the default).
umask 077

if [[ -d $HOME/.config/env.d ]]; then
	for env in "$HOME"/.config/env.d/*.conf; {
		[[ -f $env && -r $env ]] &&
			source "$env"
	}
	unset -v env
fi

if [[ -n $XDG_CONFIG_HOME ]]; then
	BASH_COMPLETION_USER_FILE=$XDG_CONFIG_HOME/bash-completion/bash_completion
	INPUTRC=$XDG_CONFIG_HOME/bash/inputrc

	declare -x BASH_COMPLETION_USER_FILE INPUTRC
fi

FIGNORE=.:lost+found:..
HISTCONTROL=ignoreboth:erasedups
HISTFILE=/dev/null
HISTFILESIZE=0
HISTSIZE=-1
HISTTIMEFORMAT=$'\e[2m[%F_%X]\e[0m '
TIMEFORMAT=$'\n\e[2mTook\e[0m %1R \e[2msecond(s)...\e[0m'

declare -x HISTCONTROL HISTFILE HISTFILESIZE \
	HISTSIZE HISTTIMEFORMAT TIMEFORMAT FIGNORE

if [[ $UID -ne 0 && -z $SSH_TTY ]]; then
	ssh_a=$HOME/.ssh/ssh-agent

	[[ -f $ssh_a ]] ||
		ssh-agent -s > "$ssh_a"
	eval $(< "$ssh_a") >/dev/null

	if ! kill -0 $SSH_AGENT_PID 2>&-; then
		ssh-agent -s > "$ssh_a"
		eval $(< "$ssh_a") >/dev/null
	fi
	unset -v ssh_a

	ssh-add "$HOME"/.ssh/Codeberg &>/dev/null

	[[ $XDG_VTNR -eq 1 && -z $DISPLAY ]] &&
		exec xinit X -- vt1 &>/dev/null
fi

[[ -f $HOME/.bashrc && -r $HOME/.bashrc ]] &&
	source "$HOME/.bashrc"
