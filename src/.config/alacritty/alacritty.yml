#------------------------------------------------------------------------------
# Description: Configuration for Alacritty
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Dotfiles
# Last Modified: 2022-09-28
#------------------------------------------------------------------------------

window:
  # Allow terminal applications to change Alacritty's window title.
  dynamic_title: false

scrolling:
  # Scrolling distance multiplier.
  multiplier: 5

# Font configuration
font:
  # Normal (roman) font face
  normal:
    # Font family
    family: IBM 3270 Nerd Font Mono

    # The `style` can be specified to pick a specific face.
    style: Regular

  # Bold font face
  bold:
    # The `style` can be specified to pick a specific face.
    style: Bold

  # Italic font face
  italic:
    # The `style` can be specified to pick a specific face.
    style: Italic

  # Bold italic font face
  bold_italic:
    # The `style` can be specified to pick a specific face.
    style: Bold Italic

  # Point size
  size: 15

schemes:
  ides3rt-dark: &ides3rt-dark
    # Default colors
    primary:
      background: '#000000'
      foreground: '#dddddd'

    # Search colors
    #
    # Colors used for the search bar and match highlighting.
    search:
      # Allowed values are CellForeground/CellBackground, which reference the
      # affected cell, or hexadecimal colors like #ff00ff.
      matches:
        foreground: '#d7d787'
        background: CellBackground
      focused_match:
        foreground: '#d7d787'
        background: CellBackground

      bar:
        background: '#000000'
        foreground: '#dddddd'

    # Normal colors
    normal:
      black:   '#000000'
      red:     '#cc241d'
      green:   '#689d6a'
      yellow:  '#d7d700'
      blue:    '#0073e5'
      magenta: '#b16286'
      cyan:    '#5cc9ff'
      white:   '#aaaaaa'

    # Bright colors
    bright:
      black:   '#555555'
      red:     '#fb4934'
      green:   '#8ec07c'
      yellow:  '#d7d787'
      blue:    '#3584e4'
      magenta: '#d3869b'
      cyan:    '#87d7ff'
      white:   '#ffffff'

  solarized-dark: &solarized-dark
    # Default colors
    primary:
      background: '#002b36'
      foreground: '#93a1a1'

    # Search colors
    #
    # Colors used for the search bar and match highlighting.
    search:
      # Allowed values are CellForeground/CellBackground, which reference the
      # affected cell, or hexadecimal colors like #ff00ff.
      matches:
        foreground: '#b58900'
        background: CellBackground
      focused_match:
        foreground: '#b58900'
        background: CellBackground

      bar:
        background: '#002b36'
        foreground: '#93a1a1'

    # Normal colors
    normal:
      black:   '#002b36'
      red:     '#dc322f'
      green:   '#859900'
      yellow:  '#b58900'
      blue:    '#268bd2'
      magenta: '#6c71c4'
      cyan:    '#2aa198'
      white:   '#93a1a1'

    # Bright colors
    bright:
      black:   '#657b83'
      red:     '#dc322f'
      green:   '#859900'
      yellow:  '#b58900'
      blue:    '#268bd2'
      magenta: '#6c71c4'
      cyan:    '#2aa198'
      white:   '#fdf6e3'

  solarized-light: &solarized-light
    # Default colors
    primary:
      background: '#fdf6e3'
      foreground: '#586e75'

    # Search colors
    #
    # Colors used for the search bar and match highlighting.
    search:
      # Allowed values are CellForeground/CellBackground, which reference the
      # affected cell, or hexadecimal colors like #ff00ff.
      matches:
        foreground: '#b58900'
        background: CellBackground
      focused_match:
        foreground: '#b58900'
        background: CellBackground

      bar:
        background: '#fdf6e3'
        foreground: '#586e75'

    # Normal colors
    normal:
      black:   '#fdf6e3'
      red:     '#dc322f'
      green:   '#859900'
      yellow:  '#b58900'
      blue:    '#268bd2'
      magenta: '#6c71c4'
      cyan:    '#2aa198'
      white:   '#657b83'

    # Bright colors
    bright:
      black:   '#93a1a1'
      red:     '#dc322f'
      green:   '#859900'
      yellow:  '#b58900'
      blue:    '#268bd2'
      magenta: '#6c71c4'
      cyan:    '#2aa198'
      white:   '#002b36'

# Colors scheme.
colors: *solarized-light

selection:
  # When set to `true`, selected text will be copied to the primary clipboard.
  save_to_clipboard: true

cursor:
  # Cursor style
  style:
    # Cursor shape
    #
    # Values for `shape`:
    #   - ▇ Block
    #   - _ Underline
    #   - | Beam
    shape: Beam

    # Cursor blinking state
    #
    # Values for `blinking`:
    #   - Never: Prevent the cursor from ever blinking
    #   - Off: Disable blinking by default
    #   - On: Enable blinking by default
    #   - Always: Force the cursor to always blink
    blinking: Never

  # If this is `true`, the cursor will be rendered as a hollow box when the
  # window is not focused.
  unfocused_hollow: false

  # Thickness of the cursor relative to the cell width as floating point number
  # from `0.0` to `1.0`.
  thickness: 0

# Shell
#
# You can set `shell.program` to the path of your favorite shell, e.g.
# `/bin/fish`. Entries in `shell.args` are passed unmodified as arguments to the
# shell.
#
# Default:
#   - (macOS) /bin/bash --login
#   - (Linux/BSD) user login shell
#   - (Windows) powershell
shell:
  program: bash
  args:
    - -p

mouse:
  # If this is `true`, the cursor is temporarily hidden when typing.
  hide_when_typing: true

key_bindings:
  - { key: Return, mods: Control, action: SpawnNewInstance }

  - { key: Up, mods: Shift, mode: ~Search, action: ScrollLineUp }
  - { key: Down, mods: Shift, mode: ~Search, action: ScrollLineDown }

  - { key: Key4, mode: Vi|~Search, action: Last }

debug:
  # Keep the log file after quitting Alacritty.
  persistent_logging: true

  # Log level
  #
  # Values for `log_level`:
  #   - Off
  #   - Error
  #   - Warn
  #   - Info
  #   - Debug
  #   - Trace
  log_level: Error
