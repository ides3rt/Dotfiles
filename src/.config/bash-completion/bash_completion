#!/usr/bin/env -S bash -p
#------------------------------------------------------------------------------
# Description: A collection of Bash-Completions
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Dotfiles
# Last Modified: 2022-08-13
#------------------------------------------------------------------------------

# Bash-completion for doas(1).
_doas ()
{
	local cur prev words cword split i
	_init_completion -s ||
		return 0

	for (( i = 1; i <= cword; i++ )); {
		[[ ${words[i]} != -* ]] && {
			local PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin/usr/bin:/sbin:/bin
			local root_command=${words[i]}
			_command_offset "$i"
			return 0
		}

		[[ ${words[i]} == -@(!(-*)[LnsCu]) ]] &&
			(( i++ ))
	}

	case $prev in
	-!(-*)u)
		COMPREPLY=($(compgen -u -- "$cur"))
		return 0 ;;

	-!(-*)C)
		_filedir ;;

	-!(-*)[Lns])
		return 0 ;;
	esac

	$split &&
		return 0

	[[ $cur == -* ]] && {
		COMPREPLY=(
			$(compgen -W '$(_parse_usage "$1")' -- "$cur")
		)

		[[ ${COMPREPLY-} == *= ]] &&
			compopt -o nospace
		return 0
	}
} && complete -F _doas doas
