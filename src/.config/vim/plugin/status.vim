"------------------------------------------------------------------------------
" Description: Toggle between Quiet and Verbose mode
" Author: iDes3rt <ides3rt@protonmail.com>
" Source: https://codeberg.org/ides3rt/Dotfiles
" Last Modified: 2022-04-17
"------------------------------------------------------------------------------

if exists('g:loaded_status_plugin')
	finish
endif

func! ToggleStatus()
	if !exists('g:show_status')
		let g:show_status = 1

		if has('syntax')
			set nocursorline
			set colorcolumn=0
		endif

		set nonumber
		set norelativenumber

		if has('cmdline_info')
			set noruler
		endif

		if has('statusline')
			set laststatus=1
		endif

	elseif g:show_status == 1
		unlet g:show_status

		if has('syntax')
			set cursorline
			set colorcolumn=+1
		endif

		set number
		set relativenumber

		if has('cmdline_info')
			set ruler
		endif

		if has('statusline')
			set laststatus=2
		endif

	endif
endfunc
call ToggleStatus()

nnoremap <silent> <Leader>ts :call ToggleStatus()<CR>
let g:loaded_status_plugin = 1
