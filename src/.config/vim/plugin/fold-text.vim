"------------------------------------------------------------------------------
" Description: A better way to handle FoldText in Vim
" Author: iDes3rt <ides3rt@protonmail.com>
" Source: https://codeberg.org/ides3rt/Dotfiles
" Last Modified: 2022-04-17
"------------------------------------------------------------------------------

if exists('g:loaded_foldtext_plugin')
	finish
endif

func! BetterFoldText()
	let l:lpadding = &foldcolumn
	redir => l:signs
		exec 'sil sign place buffer=' . bufnr('%')
	redir End
	let l:lpadding += l:signs =~ 'id=' ? 2:0

	if has('relativenumber')
		if (&number)
			let l:lpadding += max([&numberwidth, strlen(line('$'))]) + 1
		elseif (&relativenumber)
			let l:lpadding += max([&numberwidth, strlen(v:foldstart - line('w0')), strlen(line('w$') - v:foldstart), strlen(v:foldstart)]) + 1
		endif
	else
		if (&number)
			let l:lpadding += max([&numberwidth, strlen(line('$'))]) + 1
		endif
	endif

	let l:start = substitute(getline(v:foldstart), '\t', repeat(' ', &tabstop), 'g')
	let l:end = substitute(substitute(getline(v:foldend), '\t', repeat(' ', &tabstop), 'g'), '^\s*', '', 'g')

	let l:info = ' (' . (v:foldend - v:foldstart) . ')'
	let l:infolen = strlen(substitute(l:info, '.', 'x', 'g'))
	let l:width = winwidth(0) - l:lpadding - l:infolen

	let l:separator = ' … '
	let l:separatorlen = strlen(substitute(l:separator, '.', 'x', 'g'))
	let l:start = strpart(l:start , 0, l:width - strlen(substitute(l:end, '.', 'x', 'g')) - l:separatorlen)
	let l:text = l:start . ' … ' . l:end

	return l:text . repeat(' ', l:width - strlen(substitute(l:text, ".", "x", "g"))) . l:info
endfunc

set foldtext=BetterFoldText()
let g:loaded_foldtext_plugin = 1
