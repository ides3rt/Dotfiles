"------------------------------------------------------------------------------
" Description: Smarttab, but actually smart
" Author: iDes3rt <ides3rt@protonmail.com>
" Source: https://codeberg.org/ides3rt/Dotfiles
" Last Modified: 2022-04-17
"------------------------------------------------------------------------------

if exists('g:loaded_tabspace_plugin')
	finish
endif

func! TabSpace()
	if strpart(getline('.'), 0, col('.') - 1) =~ '^\t*$'
		return "\<Tab>"
	endif

	if &softtabstop > 0
		let l:tabstop = &softtabstop
	elseif &softtabstop < 0 && &shiftwidth > 0
		let l:tabstop = &shiftwidth
	else
		let l:tabstop = &tabstop
	endif

	let l:space = (virtcol('.') % l:tabstop)
	if l:space == 0
		let l:space = l:tabstop
	endif

	return repeat(' ', 1 + l:tabstop - l:space)
endfunc

inoremap <silent> <expr> <Tab> TabSpace()
let g:loaded_tabspace_plugin = 1
