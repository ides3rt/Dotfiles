"------------------------------------------------------------------------------
" Description: Toggle Textwidth
" Author: iDes3rt <ides3rt@protonmail.com>
" Source: https://codeberg.org/ides3rt/Dotfiles
" Last Modified: 2022-04-17
"------------------------------------------------------------------------------

if exists('g:loaded_textwidth_plugin')
	finish
endif

func! ToggleTextwidth()
	if !exists('g:use_textwidth')
		let g:use_textwidth = 1
		let g:my_textwidth = &textwidth
		setlocal textwidth=0

	elseif g:use_textwidth == 1
		unlet g:use_textwidth
		exec 'setlocal textwidth=' . g:my_textwidth
		unlet g:my_textwidth

	endif
endfunc

nnoremap <silent> <Leader>tw :call ToggleTextwidth()<CR>
let g:loaded_textwidth_plugin = 1
