# iDes3rt Color Scheme

This color scheme, _iDes3rt_, is heavily inspired/based on [TFL-Subtle][].

[TFL-Subtle]: https://github.com/terminalforlife/VimConfig/blob/master/source/colors/tfl-subtle.vim

Attention to Nvim-Treesitter Users
----------------------------------

It's recommended—by me—to disable Nvim-Treesitter for Bash and VimL, as I found
 Vim syntax-highlighting to be better in those languages.
