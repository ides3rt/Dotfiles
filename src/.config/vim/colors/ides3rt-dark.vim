"------------------------------------------------------------------------------
" Description: A really dark Vim color scheme
" Author: iDes3rt <ides3rt@protonmail.com>
" Source: https://codeberg.org/ides3rt/Dotfiles
" Last Modified: 2022-08-11
"------------------------------------------------------------------------------

set background=dark
if exists('g:colors_name')
	highlight clear

	if exists('syntax_on')
		syntax reset
	endif
endif

let g:colors_name = 'ides3rt-dark'

hi NonText ctermfg=254
hi Directory ctermfg=254
hi ModeMsg ctermfg=254
hi LineNr ctermfg=238
hi CursorLineNR cterm=NONE ctermfg=245
hi Question ctermfg=254
hi StatusLine cterm=bold ctermfg=243 ctermbg=233
hi Visual ctermfg=186 ctermbg=NONE
hi DiffAdd ctermfg=1 ctermbg=NONE
hi DiffDelete ctermfg=2 ctermbg=NONE
hi SignColumn ctermbg=233
hi Conceal ctermfg=254 ctermbg=NONE
hi SpellBad cterm=underline ctermbg=NONE
hi SpellCap cterm=underline ctermbg=NONE
hi Pmenu ctermfg=254 ctermbg=NONE
hi PmenuSel ctermfg=117 ctermbg=NONE
hi TabLineSel ctermfg=243 ctermbg=235
hi CursorLine cterm=NONE ctermbg=233
hi ColorColumn ctermfg=254 ctermbg=233
hi Cursor cterm=reverse
hi Normal ctermfg=102 ctermbg=16
hi Error ctermfg=254 ctermbg=NONE
hi Todo cterm=bold,reverse ctermfg=NONE ctermbg=NONE
hi Constant ctermfg=254
hi Number ctermfg=216
hi Identifier ctermfg=109
hi Statement ctermfg=250
hi Include ctermfg=167
hi PreProc ctermfg=254
hi Type ctermfg=103
hi Special ctermfg=254
hi Delimiter ctermfg=108
hi SpecialComment ctermfg=254
hi Comment ctermfg=242
hi Underlined cterm=NONE ctermfg=117
hi Ignore ctermfg=254

hi! link CursorColumn CursorLine
hi! link ErrorMsg Error
hi! link FoldColumn Folded
hi! link Folded StatusLine
hi! link MoreMsg ModeMsg
hi! link NormalNC Normal
hi! link Search Visual
hi! link SpecialKey Special
hi! link StatusLineNC StatusLine
hi! link String Normal
hi! link TabLine StatusLine
hi! link TabLineFill TabLine
hi! link TermCursor Cursor
hi! link TermCursorNC TermCursor
hi! link Title StatusLine
hi! link VertSplit StatusLine
hi! link VisualNC Visual
hi! link VisualNOS Visual
hi! link WarningMsg ErrorMsg
hi! link Whitespace Comment
hi! link WildMenu StatusLine
hi! link lCursor Cursor

hi clear DiffChange
hi clear DiffText
hi clear MatchParen
hi clear PmenuSbar
hi clear PmenuThumb
hi clear SpellLocal
hi clear SpellRare

hi Trail ctermbg=9
match Trail '\s\+$'

hi! link bashSpecialVariables Identifier
hi! link kshSpecialVariables Identifier

hi shExprRegion ctermfg=254
hi shFunction ctermfg=254
hi! link shCommandSubBQ Normal
hi! link shFunctionKey Statement
hi! link shQuote String

hi vimAddress ctermfg=254
hi vimFunction ctermfg=254
hi vimOperParen ctermfg=254
hi vimSubstDelim ctermfg=254
hi! link vimUserFunc Function

hi netrwDir ctermfg=9
hi netrwPlain ctermfg=15
hi netrwSpecial ctermfg=13
hi netrwSymLink ctermfg=14
hi netrwExe ctermfg=11
hi! link netrwClassify Normal
hi! link netrwLink Normal
hi! link netrwTreeBar Normal

hi TSVariable ctermfg=254
hi! link TSFuncBuiltin Statement
hi! link TSNamespace Type
hi! link TSNote Todo
hi! link TSPunctDelimiter Statement
hi! link TSWarning Todo
hi! link vimTSConstant Type
