-------------------------------------------------------------------------------
-- Description: Configuration for NetRW
-- Author: iDes3rt <ides3rt@protonmail.com>
-- Source: https://codeberg.org/ides3rt/Dotfiles
-- Last Modified: 2022-08-11
-------------------------------------------------------------------------------

local let = vim.g

let.netrw_preview = 1
let.netrw_alto = 0
let.netrw_altv = 0
let.netrw_banner = 0
let.netrw_browse_split = 4
let.netrw_cursor = 0
let.netrw_dirhistmax = 0
let.netrw_errorlvl = 2
let.netrw_liststyle = 3
let.netrw_sizestyle = 'H'
let.netrw_usetab = 1
let.netrw_winsize = 80

vim.cmd [[
	au FileType netrw setlocal bufhidden=wipe
	au WinEnter * if winnr('$') == 1 && getbufvar(winbufnr(winnr()), "&filetype") == 'netrw' | quit | endif
]]

vim.api.nvim_set_keymap(
	'n', '<Leader>.', ':Lex<Bar>vert res 38<CR>',
	{ noremap = true, silent = true }
)
