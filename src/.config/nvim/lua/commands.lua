-------------------------------------------------------------------------------
-- Description: Configuration for custom-command in Neovim
-- Author: iDes3rt <ides3rt@protonmail.com>
-- Source: https://codeberg.org/ides3rt/Dotfiles
-- Last Modified: 2022-06-25
-------------------------------------------------------------------------------

vim.cmd [[
	if executable('sudo') && executable('tee')
		com W exec 'w !sudo -A tee % >/dev/null' | edit!
	endif
]]
