-------------------------------------------------------------------------------
-- Description: Manage Neovim-filetypes
-- Author: iDes3rt <ides3rt@protonmail.com>
-- Source: https://codeberg.org/ides3rt/Dotfiles
-- Last Modified: 2022-08-11
-------------------------------------------------------------------------------

vim.cmd [[
	" Set correct file-type.
	au BufNewFile,BufRead *.hook,*.theme setf dosini

	" Remove trailing whitespaces.
	au BufWritePre * :%s/\s\+$//e

	" Most of the time files that use '#' as a comments format,
	" is not set 'comments' and 'commentstring' properly.
	au FileType * setl com=:# cms=#%s

	" Disable Shada, undo-file, backups,
	" and swaps on temporarily files.
	au BufNewFile,BufRead *.tmp,*/tmp/* setl sdf=NONE nobk noswf noudf

	" I use 'sudoers.local' for my usual settings.
	au BufNewFile,BufRead */etc/sudoers.local,sudoers.local.tmp setf sudoers

	" Correctly set comments in Xdefaults/Xmodmap.
	au FileType xdefaults,xmodmap setl com=:! cms=!%s
]]

-- I don't like Rust recommended style.
vim.g.rust_recommended_style = 0
