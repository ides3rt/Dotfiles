-------------------------------------------------------------------------------
-- Description: Neovim colors-related configurations
-- Author: iDes3rt <ides3rt@protonmail.com>
-- Source: https://codeberg.org/ides3rt/Dotfiles
-- Last Modified: 2022-12-17
-------------------------------------------------------------------------------

local set = vim.opt

if os.getenv('DISPLAY') == nil then
	vim.cmd('au VimEnter * colorscheme zen')

else
	set.termguicolors = true
	vim.cmd('au VimEnter * colorscheme zen-light')
end

if vim.fn.has('gui_running') == 0 then
	set.guicursor = ''
end

set.lazyredraw = true
set.redrawtime = 10000
set.regexpengine = 1
