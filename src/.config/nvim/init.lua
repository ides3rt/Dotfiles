-------------------------------------------------------------------------------
-- Description: Initialize Neovim in LuaJIT
-- Author: iDes3rt <ides3rt@protonmail.com>
-- Source: https://codeberg.org/ides3rt/Dotfiles
-- Last Modified: 2022-09-28
-------------------------------------------------------------------------------

require('filetypes')
require('options')
require('keymaps')
require('commands')
require('colors')
require('plugin/netrw')

vim.opt.secure = true
