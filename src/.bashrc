#!/usr/bin/env -S bash -p
#------------------------------------------------------------------------------
# Description: ~/.bashrc
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Dotfiles
# Last Modified: 2022-12-26
#------------------------------------------------------------------------------

# Automatically exits under X is an annoyance, unless I'm root.
[[ $UID -eq 0 || -z $DISPLAY ]] &&
	declare -xr TMOUT=300

{
	[[ $- != *i* ]] ||
		shopt -q restricted_shell
} && return 0

dev_mux ()
{
	type -P tmux &>/dev/null ||
		return 1

	{ tmux attach ||
		tmux new \; splitw -h \; splitw -v \; selectp -t 1
	} 2>/dev/null
}

if [[ $UID -ne 0 && -z $TMUX ]]; then
	[[ $(dev_mux) == '[exited]' ]] &&
		exit 0
fi

set +o monitor
shopt -s autocd cdspell checkhash direxpand dirspell dotglob \
	extglob globasciiranges globstar histappend hostcomplete \
	lithist no_empty_cmd_completion progcomp_alias xpg_echo

shopt -u expand_aliases
enable -n alias unalias

for bash in "$XDG_CONFIG_HOME"/bash/!(inputrc) \
	/usr/share/bash-completion/bash_completion
{
	[[ -f $bash && -r $bash ]] &&
		source "$bash"
}
unset -v bash
